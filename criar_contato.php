<?php

require 'conexao.php';
require 'sistema.php';

//Fazer o upload da imagem
// if(isset($_FILES['imagem'])){
//     $imagem = $_FILES['imagem']['name'];
//     $extensao = strtolower(pathinfo($imagem, PATHINFO_EXTENSION));
//     $nome_imagem = md5(time().".".$extensao);

//     move_uploaded_file($_FILES['imagem']['tmp_name'], 'agenda-de-contatos/imagens/' . $nome_imagem);

//     $sql = 'INSERT INTO contatos(imagem) VALUES(:nome_imagem)';
//     $stmt = $conexao->prepare($sql);
//     $stmt->execute([':nome_imagem' => $imagem]);
// }
//Se existir nome, celular e e-mail, e se eles não estiverem vazios
if(isset($_POST['nome']) && isset($_POST['celular']) && isset($_POST['email']) && 
    isset($_POST['genero']) && isset($_POST['nascimento']) && isset($_POST['grupo']) &&
    isset($_POST['comentario'])){

    //Guarda as variaveis que vieram do formulário
    $nome = $_POST['nome'];
    $celular = $_POST['celular'];
    $email = $_POST['email'];
    $genero = $_POST['genero'];
    $nascimento = $_POST['nascimento'];
    $grupo = $_POST['grupo'];
    $comentario = $_POST['comentario'];

    //Insere elas na tabela contatos
    $sql = 'INSERT INTO contatos(nome, celular, email, genero, nascimento, grupo, comentario) 
            VALUES(:nome, :celular, :email, :genero, :nascimento, :grupo, :comentario)';
    //Declara a conexão e passa como parâmetro a variavel sql
    $stmt = $conexao->prepare($sql);
    
    //O operador => é usado para atribuir os valores // as variaveis
    if($stmt->execute([ ':nome' => $nome, ':celular' => $celular, ':email' => $email, 
                        'genero' => $genero, 'nascimento' => $nascimento, 'grupo' => $grupo, 
                        'comentario' => $comentario])){

        header('Location: /agenda-de-contatos/index.php');

    }

}

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Criar Contato</title>

</head>

<body>

    <div class="container">

        <div class="jumbotron border-0">
            <h1>CRIAR CONTATO</h1>       
        </div>

        <br><br>

        <!-- Formulário para o envio de foto do contato 
        <form action="criar_contato.php" method="POST" enctype="multipart/form-data">
            <div class="row">
                <div class="col-9">
                    <label for="imagem">Adicionar foto do contato:</label>
                    <input type="file" name="imagem" id="imagem">
                </div>
            </div>
        </form>-->
        <div class="container">
            <form method="POST" name="form_criar_contato">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="nome">Nome</label>
                            <input class="form-control" type="text" name="nome" id="nome" required>
                        </div>

                        <div class="col-md-6">
                            <label for="celular">Celular</label>
                            <input class="form-control" type="text" name="celular" id="celular" required>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="email">E-mail</label>
                            <input class="form-control" type="email" name="email" id="email" required>
                        </div>

                        <div class="col-md-6">
                            <div class="form-check form-check-inline" style="margin-bottom: 1em;">
                                <label class="form-check-label" for="genero">Gênero:</label>
                            </div>
                            <br />
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="genero" id="masculino" value="Masculino">
                                <label class="form-check-label" for="marculino">Masculino</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="genero" id="feminino" value="Feminino">
                                <label class="form-check-label" for="feminino">Feminino</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="genero" id="Outro" value="Outro">
                                <label class="form-check-label" for="outro">Outro</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">

                        <div class="col-md-6">
                            <label for="nascimento">Data de nascimento:</label>
                            <input class="form-control" type="date" name="nascimento" id="nascimento">
                        </div>

                        <div class="col-md-6">
                            <label for="grupo" style="margin-bottom: 1em;">Grupo:</label>
                            <br />
                            <input type="checkbox" name="grupo" id="grupo" value="Familia">
                            <label for="grupo" style="margin-bottom: 1em;">Família</label>
                            <input type="checkbox" name="grupo" id="grupo" value="Familia">
                            <label for="grupo">Amigos</label>
                            <input type="checkbox" name="grupo" id="grupo" value="Trabalho">
                            <label for="grupo">Trabalho</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="comentario">Comentário:</label><br>
                            <textarea class="form-control" name="comentario" id="comentario" cols="30" rows="10"></textarea>
                        </div>

                    </div>
                </div>

                <div class="col mt-4 md-4">
                    <button class="btn btn-success" type="submit">Salvar</button>
                    <a class="btn btn-primary" href="/agenda-de-contatos/index.php">Voltar</a>
                </div>

            </form>
        </div>
    </div>

</body>
</html>