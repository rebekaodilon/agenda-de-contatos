<?php

require 'conexao.php';
require 'sistema.php';



?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>

    <link href="js/login.js" rel="stylesheet">
    <link href="css/login.css" rel="stylesheet">
</head>
<body>
    <div class="login-page">
        <div class="form">
            <form class="register-form" method="POST" style="display: none;">
                
                <input type="text" name="nome_cadastro" id="nome_cadastro" placeholder="Nome" required/>
                <input type="password" name="senha_cadastro" id="senha_cadastro" placeholder="Senha" required/>
                <input type="text" name="email" id="email" placeholder="E-mail" required/>
                
                <button type="button" name="btn-cadastrar" onclick="registrar()">Criar</button>
                <p class="message">Já possuí uma conta? <a href="#" id="login">Login</a></p>
            </form>
            <form class="login-form" method="POST">
                
                <input type="text" name="username" id="username" placeholder="Nome de usuário" required/>
                <input type="password" name="senha" id="senha" placeholder="Senha" required/>
                
                <button type="button" name="btn-login" onclick="login()">login</button>
                <p class="message">Não cadastrado? <a href="#" id="criar_conta">Criar uma conta</a></p>
            </form>
        </div>
    </div>
</body>
</html>

<script type="text/javascript">

    if ($('.login-form').css('display', 'block')) {
        $('#criar_conta').on('click', function(){
            $('.login-form').css('display', 'none');
            $('.register-form').css('display', 'block');
        });
    } 
    if ($('.register-form').css('display', 'none')) {
        $('#login').on('click', function(){
            $('.register-form').css('display', 'none');
            $('.login-form').css('display', 'block');
        });
    }

    

</script>