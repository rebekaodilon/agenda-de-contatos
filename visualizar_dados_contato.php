<?php

require 'conexao.php';
require 'sistema.php';

$id = $_GET['id'];


$sql = 'SELECT * FROM contatos WHERE id=:id';

$stmt = $conexao->prepare($sql);

$stmt->execute([':id' => $id]);

$contato = $stmt->fetch(PDO::FETCH_OBJ);



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detalhes do contato</title>

</head>
<body>
    
    <div class="container">

        <div class="jumbotron border-0">
            <h1>DETALHES</h1>       
        </div>
        
        <div class="form-group">
            <div class="row">
                
                <div class="col-md-3"></div>
                <div class="col-md-8">
                    <form class="" action="" method="POST" name="form_criar_contato">
                        
                        <div class="form-group">
                            <div class="row">
                                
                                <div class="col-6">
                                    <h4>Nome:</h4>
                                    <input type="text" value="<?= $contato->nome; ?>" readonly>
                                </div>
                                
                                <div class="col-6">                
                                    <h4>Celular:</h4>
                                    <input type="text" value="<?= $contato->celular; ?>" readonly>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                
                                <div class="col-6">
                                    <h4>E-mail:</h4>
                                    <input type="text" value="<?= $contato->email; ?>" readonly>
                                </div>
                                
                                <div class="col-6">
                                    <h4>Gênero:</h4>
                                    <input type="text" value="<?= $contato->genero; ?>" readonly>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                
                                <div class="col-6">
                                    <h4>Data de Nascimento:</h4>
                                    <input type="text" value="<?= $contato->nascimento; ?>" readonly>
                                </div>
                                
                                <div class="col-6">
                                    <h4>Grupo:</h4>
                                    <input type="text" value="<?= $contato->grupo; ?>" readonly>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                
                                <div class="col-6">
                                    <h4>Comentário:</h4>
                                    <input type="text" value="<?= $contato->comentario; ?>" readonly>
                                </div>
                                
                            </div>
                        </div>
                    </form>
                    <div class="col-md-2"></div> 
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col mt-4">
                    <a class="btn btn-primary" href="/agenda-de-contatos/index.php">Voltar</a>
                </div>
                <div class="col-md-8"></div>
            </div>
        </div>
    </div>

</body>
</html>

<style>
    input{
        border: 0;
        width: 100%;
    }
    textarea:focus, input:focus{
        outline: none;
    }
</style>