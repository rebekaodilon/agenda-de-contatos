<?php

require 'conexao.php';

$id = $_GET['id'];

$sql = 'DELETE FROM contatos WHERE id=:id';

$stmt = $conexao->prepare($sql);

$stmt->execute([':id' => $id]);

header('Location: /agenda-de-contatos/index.php');

?>