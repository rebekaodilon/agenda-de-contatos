<?php
//Chama o arquivo para que possa usar a variavel conexao
require 'conexao.php';
require 'sistema.php';

$sql = 'SELECT * FROM contatos;';

$stmt = $conexao->prepare($sql);

$stmt->execute();

$contatos = $stmt->fetchAll(PDO::FETCH_OBJ);

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Página Inicial</title>

</head>
<body>

    <div class="container">

    <div class="jumbotron border-0">
            <h1>CONTATOS</h1>       
        </div>

        <div class="form-group">
        
            <div class="row">
                <div class="col-md-5"></div>
                <div class="col-md-5"></div>
                <div class="col-md-2">
                    <a href="criar_contato.php" class="btn btn-primary btn-sm" >Criar Contato</a>
                </div>
            </div>
        </div>

        <h3>LISTA DE CONTATOS</h3>
        
        <?php foreach($contatos as $contato): ?>
        
            <div class="form-group">
            
                <div class="row">

                    <div class="col-sm-12 mt-4">

                        <li class="list-group-item">
                            <?= $contato->nome; ?>
                        </li>

                    </div>
                </div>
            </div>

            <div class="form-group">
            
                <div class="row" style="margin-top: -1em;">

                    <div class="col-sm-4">

                        <li class="list-group-item">

                            <a class="btn btn-warning" href="editar_contato.php?id=<?= $contato->id?>">Editar</a>
                            <a class="btn btn-danger" href="excluir_contato.php?id=<?= $contato->id?>">Excluir</a>
                            <a class="btn btn-info" href="visualizar_dados_contato.php?id=<?= $contato->id?>">Ver mais</a>

                        </li>

                    </div>

                </div>
            
            <?php endforeach; ?>
             
        </div>
        
    </div>

</body>
</html>