<?php

require 'conexao.php';
require 'sistema.php';

$id = $_GET['id'];

$sql = 'SELECT * FROM contatos WHERE id=:id';

$stmt = $conexao->prepare($sql);

$stmt->execute([':id' => $id]);

$contato = $stmt->fetch(PDO::FETCH_OBJ);

//Se o usuário clilcar em salvar entra no if
if(isset($_POST['nome']) && isset($_POST['celular']) && isset($_POST['email']) && 
    isset($_POST['genero']) && isset($_POST['nascimento']) && isset($_POST['grupo']) &&
    isset($_POST['comentario'])){

    //Guarda as variaveis que vieram do formulário
    $nome = $_POST['nome'];
    $celular = $_POST['celular'];
    $email = $_POST['email'];
    $genero = $_POST['genero'];
    $nascimento = $_POST['nascimento'];
    $grupo = $_POST['grupo'];
    $comentario = $_POST['comentario'];

    //Altera elas na tabela contatos
    $sql = "UPDATE contatos 
            SET nome='$nome', celular='$celular', email='$email', genero='$genero', nascimento='$nascimento', grupo='$grupo', comentario='$comentario'
            WHERE id='$id'";
    //Declara a conexão e passa como parâmetro a variavel sql
    $stmt = $conexao->prepare($sql);
    
    //O operador => é usado para atribuir os valores
    if($stmt->execute(['nome' => $nome, 'celular' => $celular, 'email' => $email, 'genero' => $genero, 'nascimento' => $nascimento, 'grupo' => $grupo, 'comentario' => $comentario])){

        header('Location: /agenda-de-contatos/index.php');

    }

}

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Contato</title>

</head>

<body>

    <div class="container">

        <div class="jumbotron border-0">
            <h1>Editar</h1>       
        </div>

        <form action="" method="POST" name="form_editar_contato">
            <div class="row justify-content-md-center">
                <div class="col">
                    <label for="nome">Nome</label><br>
                    <input class="form-control" value="<?= $contato->nome; ?>" type="text" name="nome" id="nome">
                </div>
                
                <div class="col">                    
                    <label for="celular">Celular</label><br>
                    <input class="form-control" value="<?= $contato->celular; ?>" type="number" name="celular" id="celular">                    
                </div>
            </div>
            <div class="row justify-content-md-center">
                
                <div class="col-6">                    
                    <label for="email">E-mail</label><br>
                    <input class="form-control" value="<?= $contato->email; ?>" type="email" name="email" id="email">                    
                </div>

                <div class="col-6">                    
                    <label for="genero">Gênero</label><br>
                    <input class="form-control" value="<?= $contato->genero; ?>" type="genero" name="genero" id="genero">
                </div>
            </div>
            <div class="row justify-content-md-center">

                <div class="col-6">                   
                    <label for="nascimento">Data de nascimento</label><br>
                    <input class="form-control" value="<?= $contato->nascimento; ?>" type="nascimento" name="nascimento" id="nascimento">      
                </div>

                <div class="col-6">                    
                    <label for="grupo">Grupo</label><br>
                    <input class="form-control" value="<?= $contato->grupo; ?>" type="grupo" name="grupo" id="grupo">              
                </div>
            </div>
            <div class="row">

                <div class="col align-self-center">                    
                    <label for="comentario">Comentário</label><br>
                    <textarea class="form-control" name="comentario" id="comentario" cols="5" rows="10"><?= $contato->comentario; ?></textarea>
                </div>
            </div>
            <div class="col mt-4">
                <button class="btn btn-success" type="submit">Salvar</button>
                <a class="btn btn-primary" href="/agenda-de-contatos/index.php">Voltar</a>
            </div>
        </form>
    </div>

</body>
</html>